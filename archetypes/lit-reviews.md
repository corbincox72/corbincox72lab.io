---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
author: "Corbin Cox"
paperTypes: ["primary research","meta-analysis","review","opinion"]
systems: []
tags: []
draft: true
toc: false
math: false
markup: 'mmark' #required for katex
mermaid: false
---
