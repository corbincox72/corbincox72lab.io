---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
toc: false
math: false
markup: 'mmark' #required for katex
mermaid: false
---
