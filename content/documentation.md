---
title: "Documentation"
date: 2020-07-22T20:15:55-04:00
draft: false
katex: true
---

# KaTeX
Set parameter `katex: true`

Display Math
```
$$ MATH EXPRESSION $$
```
$$ \int \cos(x) $$

Inline Math
```
\\( MATH EXPRESSION \\)
```
\\( \int \cos(x) \\)

# Mermaid Diagrams

```
{{<mermaid>}}
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
{{</mermaid>}}
```
