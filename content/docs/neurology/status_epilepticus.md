---
title: "Status Epilepticus"
date: 2021-05-27T10:38:46-04:00
author: "Corbin Cox"
systems: [neurology]
tags: []
draft: true
toc: true
math: false
markup: 'mmark'
mermaid: false
---

# Definitions
{{% definition %}}
Continuous seizure activity or interrupted seizure activity with at least 2 seizures and without return to baseline conciousness  lasting for ...

<dl>
	<dt>Brief Seizure:</dt><dd> 5 minutes</dd>
  <dt>Prolonged Seizure:</dt><dd> 5 &ndash; 30 minutes</dd>
  <dt>Status Epilepticus:</dt><dd> &ge; 30 minutes</dd>
</dl>
{{% /definition %}}

Per the definitions above, *status epilepticus* requires 30 minutes or more of seizure activity: however, treatment is usually initiated after 5 minutes of seizure activity to prevent significantly prolonged seizures, as the risk increases substantially after 5 minutes.