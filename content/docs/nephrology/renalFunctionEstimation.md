---
title: "Renal Function Estimation"
date: 2021-08-01T14:42:14-04:00
draft: false
toc: true
math: true
markup: mmark #required for katex
mermaid: false
---

# CG-CrCL Weight Selection

Weight selection for use in the CG-CrCL equation is an essential aspect of clinical pharmacokinetics and renal function estimation. The table below illustrates a suggested method for appropriately estimating renal function in individuals of all body sizes utilizing the CG-CrCL equation:

$$
\frac{(140-\mathrm{age})\times \mathrm{wt}}{72 \times \mathrm{SCr}}\times 0.85 \text{ (if female)}
$$


<table>
  <caption>Weight Selection for CG-CrCL</caption>
  <thead>
    <tr>
      <th>Criteria</th><th>Weight to be Used</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>BMI &lt; 18.5</td><td>Actual Weight</td>
    </tr>
    <tr>
      <td>18.5 &le; BMI &le; 25</td><td>IBW</td>
    </tr>
    <tr>
      <td>Weight &ge; 1.3 &times; IBW<sup>&dagger;</sup></td><td>Adj. Weight</td>
    </tr>
    <tr>
      <td>BMI &ge; 25<sup>&dagger;</sup></td><td>Adj. Weight</td>
    </tr>
    <tr>
      <td>BMI &ge; 40</td><td>LBW<sup>&ddagger;</sup></td>
    </tr>
  </tbody>
</table>

<sup>&dagger;</sup> Across normal adult human heights, 
$$
1.3 \times \mathrm{IBW} \equiv \mathrm{BMI} \approx 25 \text{--} 26
$$

<sup>&ddagger;</sup> The use of LBW vs. Adj. Weight in this situation is debatable, but consideration of clinical scenario is imperative as neither weight produces highly reliable estimates of CrCL

# CG-CrCL Estimation in Individuals with Amputation

To calculate the necessary weights for CrCL, the following information is needed

1. Patient's *current* weight
2. Patient's *pre-amputation* adult height (if amputation would have caused reduction in measured height, i.e. bilateral BKA's)
3. Location of all amputations the patient has undergone

[This calculator from clincalc.com](https://clincalc.com/Kinetics/EBWL.aspx) may be useful. This calculator is utilizing the following data to compute values:

<table>
  <caption>&percnt; Body Weight Lost by Amputation (EBWL)</caption>
  <thead>
    <tr><th>Location</th><th>&percnt; Wt</th></tr>
  </thead>
  <tbody>
    <tr>
      <td>Foot</td><td>1.5&percnt;</td>
    </tr>
    <tr>
      <td>BKA</td><td>3.5&percnt;</td>
    </tr>
    <tr>
      <td>AKA</td><td>11&percnt;</td>
    </tr>
    <tr>
      <td>Hip Disarticulation</td><td>16&percnt;</td>
    </tr>
    <tr>
      <td>Hand</td><td>0.7&percnt;</td>
    </tr>
    <tr>
      <td>Forearm</td><td>1.5&percnt;</td>
    </tr>
    <tr>
      <td>Entire Arm</td><td>4&percnt;</td>
    </tr>
  </tbody>
</table>


## Calculations

IBW<sub>pre</sub> is calculated as normal using the patient's *pre-amputation* height

$$
\mathrm{IBW}_\mathrm{post} = \mathrm{IBW}_\mathrm{pre} \times (1-\mathrm{EBWL})
$$

$$
\text{Adj. Wt} = \mathrm{IBW}_\mathrm{post} + 0.4\left( \mathrm{Wt} - \mathrm{IBW}_\mathrm{post} \right)
$$

# Estimation of Chronic Renal Function

Estimation of chronic renal function can be most accurately be computed using the CKD-EPI equation, a copy of which can be found [here on mdcalc.com](https://www.mdcalc.com/ckd-epi-equations-glomerular-filtration-rate-gfr)
