---
title: "Hypertension"
date: 2020-08-08T16:17:05-04:00
author: "Corbin Cox"
systems: ["Cardiovascular"]
tags: [cardiovascular]
draft: false
toc: true
math: false
mermaid: false
---

# Pathophysiology

## Test Content

### Test Again

#### Once More with feeling



# Diagnosis

# Management



# References

1. 2014 JNC 8 Guidelines - https://jamanetwork.com/journals/jama/fullarticle/1791497
2. 2020 ISH Guidelines - https://www.ahajournals.org/doi/10.1161/HYPERTENSIONAHA.120.15026
3. Hypertension - Nature Review Disease Primer - https://www.nature.com/articles/nrdp201814
