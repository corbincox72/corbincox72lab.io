---
title: "Patient Assistance Programs (PAPs)"
date: 2020-08-06T11:28:16-04:00
draft: false
katex: false
mermaid: false
---

**For drugs, especially brand name drugs, always check manufacturers' websites for discounts / free trials. These are often restricted by income, and most Medicare patients are not eligible, but this varies per program and it is always worth a check.**

- [GoodRx:](https://www.goodrx.com) Free discount cards for many drugs, particularly generics. Make sure you change the brand / generic, dosage, and dosage form tabs appropriately
- [Walmart &#36;4 List:](https://www.walmart.com/cp/$4-prescriptions/1078664?povid=cat1070145-env172199-moduleA062514-lLinkGNAV_Pharmacy_Pharmacy_4DollarPrescriptions): List of all prescription medications available at Walmart with no insurance, &#36;4 for 1 month, &#36;10 for 90 days
- [Sam's Club Saving Program:](https://www.samsclub.com/content/prescription-savings?pageName=extra-value-drug-list) More expansive than Walmart &#36;4 list, but requires Sam's Club Membership
- [Cancer Care:](https://www.cancercare.org/copayfoundation#!covered-diagnoses) CoPay assistance for cancer therapies
- [Good Days:](https://www.mygooddays.org) Lists of open and closed CoPay PAPs separated by disease state
- [Healthwell Foundation:](https://www.healthwellfoundation.org/disease-funds/?fund_status%5B%5D=open) Similar to Good Days, but grants
- [Rare Diseases:](https://rarediseases.org/for-patients-and-families/help-access-medications/patient-assistance-programs-2/) PAPs for rare diseases
- [PAN Foundation:](https://www.panfoundation.org) Variety of PAPs
- [Patient Services:](https://www.patientservicesinc.org) Variety of PAPs
- [The Assistance Fund:](https://tafcares.org) Variety of PAPs