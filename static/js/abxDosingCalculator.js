function updateOutputs() {
  weightOutput();
  renalOutput();
  pkOutput();
  vdEstimation();
  push(vd(),"vdInputValue",1);
  push(vd()/dosingWeight(),"vdwtInputValue",1);
  regimenOutput();
  doubleCheckOutput();
  updateTable();
}

//Box I/O
function push(num,id,l){
  document.getElementById(id).innerHTML = parseFloat(num).toFixed(l);
}

function pushText(text,id){
  document.getElementById(id).innerHTML = text;
}

function pull(id){
  return parseFloat(document.getElementById(id).value);
}

function dropdownValue(elementId){
  let e = document.getElementById(elementId);
  let value = e.options[e.selectedIndex].value;
  return value;
}

// Change Visible Content

function updatePageContent() {
  if (drugSelection() == 'vancInitial') {
    $('.vancOnly').css('display', 'revert');
    $('.agOnly').css('display', 'none');
    $('.initialOnly').css('display', 'revert');
    $('.levelOnly').css('display', 'none');
  } else if (drugSelection() == "vancPkTr") {
    $('.vancOnly').css('display', 'revert');
    $('.agOnly').css('display', 'none');
    $('.initialOnly').css('display', 'none');
    $('.levelOnly').css('display', 'revert');
    document.getElementById("zeroForTroughNo").checked = true;
  } else if (drugSelection() == "agInitial") {
    $('.vancOnly').css('display', 'none');
    $('.agOnly').css('display', 'revert');
    $('.initialOnly').css('display', 'revert');
    $('.levelOnly').css('display', 'none');
  } else {
    $('.vancOnly').css('display', 'none');
    $('.agOnly').css('display', 'revert');
    $('.initialOnly').css('display', 'none');
    $('.levelOnly').css('display', 'revert');
    document.getElementById("zeroForTroughYes").checked = true;
  }
}

// Variable Value Update Functions

function drugSelection() {
  return dropdownValue('drugSelectionInput');
}

function sex() {
  return dropdownValue("sexInput");
}

function age() {
  return pull("ageInput");
}

function weight() {
  if (dropdownValue("weightUnitsInput") == "kg") {
    return pull("weightInput");
  } else {
    return (pull("weightInput")*2.205);
  }
}

function height() {
  if (dropdownValue("heightUnitsInput") == "in") {
    return pull("heightInput");
  } else {
    return (pull("heightInput")/2.54).toFixed(3);
  }
}

function scr() {
  return pull("scrInput");
}

function crclInput() {
  return pull("crclInput");
}

function pkMeasured() {
  return pull("pkMeasuredInput");
}

function trMeasured() {
  return pull("trMeasuredInput");
}

function pkMaxInterval() {
  return pull("pkMaxIntervalInput");
}

function trMinInterval() {
  return pull("trMinIntevalInput");
}

function pkTrInterval() {
  return pull("pkTrIntevalInput");
}

function currentDose() {
  return pull("currentDoseInput");
}

function currentTau() {
  return pull("currentTauInput");
}

function currentTinf() {
  return pull("currentTinfInput");
}

function selectedVd() {
  return pull("selectedVdInput");
}

function goalPk() {
  return pull("goalPkInput");
}

function goalTr() {
  return pull("goalTrInput");
}

function selectedTinf() {
  return pull("selectedTinfInput");
}

function selectedDose() {
  return pull("selectedDoseInput");
}

function selectedTau() {
  return pull("selectedTauInput");
}

// Body Weight Calculations
function weightOutput() {
  push(weight(),"wtValue",1);
  push(ibw(),"ibwValue",1);
  push(adjbw(),"adjwtValue",1);
  push(bmi(),"bmiValue",1);
  push(bsa(),"bsaValue",2);
}

function ibw() {
  if (sex() == "male") {
    return 50.0 + 2.3 * (height() - 60.0);
  } else {
    return 45.5 + 2.3 * (height() - 60.0);
  }
}

function adjbw() {
  return (ibw() + 0.4*(weight()-ibw()));
}

function lbw() {
  if (sex() == "male") {
    return 9270.0 * weight() / ((216.0 * bmi()) + 6680.0);
  } else {
    return 9270.0 * weight() / ((244.0 * bmi()) + 8780.0);
  }
}

function bmi() {
  return (703.0*2.205*weight() / height() / height() );
}

function bsa() {
  return ( Math.sqrt(weight() * height() * 2.54 / 3600.0) );
}

// Renal Function
function renalWeight() {
  if (weight() < ibw()) {
    pushText("(weight)","crclWeightOutput");
    return weight();
  } else if (bmi() >= 40) {
    pushText("(lbw)","crclWeightOutput");
    return lbw();
  } else if (1.3*ibw() > weight()) {
    pushText("(ibw)","crclWeightOutput");
    return ibw();
  } else {
    pushText("(adj wt.)","crclWeightOutput");
    return adjbw();
  }
}

function renalOutput() {
  push(cgCrCL(),"cgcrclCalcValue",0);
  push(crcl(),"cgcrclInputValue",0);
}

function cgCrCL() {
  if (sex() == "male") {
    return (140.0-age()) * renalWeight() / 72.0 / scr();
  } else {
    return (140.0-age()) * renalWeight() / 72.0 / scr() * 0.85;
  }
}

function crcl() {
  if (document.getElementById("crclInput").value.length == 0) {
    return cgCrCL();
  } else {
    return crclInput();
  }
}

// Drug-Specific PK
function pkOutput() {
  push(k(),"kValue",3);
  push(halfLife(),"halfLifeValue",1);
}

function dosingWeight() {
  if (drugSelection() == "agInitial" || drugSelection() == "agPkTr") {
    if (weight() < ibw()) {
      return weight();
    } else if (ibw()*1.3 <= weight() || bmi() >= 40) {
      return adjbw();
    } else {
      return ibw();
    }
  } else {
    return weight();
  }
}

function k() {
  if (drugSelection() == "agInitial") {
    return (0.0024 * crcl()) + 0.01;
  } else if (drugSelection() == "vancInitial") {
    return (0.00083 * crcl()) + 0.0044;
  } else {
    return Math.log(pkMeasured()/trMeasured())/pkTrInterval();
  }
}

function halfLife() {
  return Math.log(2)/k();
}

function vdEstimation() {
  if (drugSelection() == "agInitial" || drugSelection() == "agPkTr") {
    pushText("0.3 &ndash; 0.35","vdwtRangeValue");
    let lowerPure = 0.3 * dosingWeight();
    let upperPure = 0.35 * dosingWeight();
    let lower = parseFloat(lowerPure).toFixed(1);
    let upper = parseFloat(upperPure).toFixed(1);
    pushText(`${lower} &ndash; ${upper}`,"vdRangeValue");
  } else if (drugSelection() == "vancInitial" || drugSelection() == "vancPkTr") {
    pushText("0.6 &ndash; 0.7","vdwtRangeValue");
    let lowerPure = 0.6 * dosingWeight();
    let upperPure = 0.7 * dosingWeight();
    let lower = parseFloat(lowerPure).toFixed(1);
    let upper = parseFloat(upperPure).toFixed(1);
    pushText(`${lower} &ndash; ${upper}`,"vdRangeValue");
  } else {
    return ;
  }
}

function currentCmax(){
  return pkMeasured() * Math.exp(k() * pkMaxInterval());
}

function currentCmin(){
  return trMeasured() * Math.exp(-1.0 * k() * trMinInterval());
}

function vd() {
  if (drugSelection() == "agInitial" || drugSelection() == "vancInitial") {
    return selectedVd();
  } else if (document.getElementById("zeroForTroughYes").checked == true) {
    return [currentDose() * (1.0 - Math.exp(-1.0 * k() * currentTinf())) / (currentTinf() * k() * [currentCmax()])];
  } else if (document.getElementById("zeroForTroughNo").checked == true) {
    return [currentDose() * (1.0 - Math.exp(-1.0 * k() * currentTinf())) / (currentTinf() * k() * [currentCmax() - currentCmin() * Math.exp(-1.0 * k() * currentTinf())])];
  }
}

// Regimen Estimation
function regimenOutput() {
  push(tauEstimate(),"tauEstValue",1);
  push(doseEstimation(),"mdEstValue",0);
  push(selectedTau(),"tauInputValue",0);
  push(selectedDose(),"mdInputValue",0);
}

function tauEstimate() {
  let ans = (Math.log(goalPk() / goalTr()) / k()) + selectedTinf();
  return ans.toFixed(1);
}

function doseEstimation() {
  return (goalPk() * selectedTinf() * vd() * k() * (1.0 - Math.exp(-1.0 * k() * selectedTau())) / (1.0 - Math.exp(-1.0 * k() * selectedTinf())));
}

// Regimen Double Checks
function doubleCheckOutput() {
  push(cmaxCheck(),"cmaxValue",1);
  push(cminCheck(),"cminValue",1);
  push(auc(),"aucValue",0);
}


function cmaxCheck() {
  return (selectedDose() * (1.0 - Math.exp(-1.0 * k() * selectedTinf())) / (selectedTinf() * vd() * k() * (1.0 - Math.exp(-1.0 * k() * selectedTau()))));
}

function cminCheck() {
  return (cmaxCheck() * Math.exp(-1.0 * k() * (selectedTau() - selectedTinf())));
}

function auc() {
  return [24.0 / selectedTau() * [(cmaxCheck() + cminCheck() / 2.0  * selectedTinf()) + ((cmaxCheck() - cminCheck())/k())]];
}

// Vanc table

// Table Functions
function tinfTable(dose){
  if (dropdownValue("infusionTimeSelectionInput") == "all2Hours") {
    return 2;
  } else if (dose == 1000) {
    return 1;
  } else if (dose == 1250) {
    return 1.5;
  } else if (dose == 1500) {
    return 1.5;
  } else if (dose == 1750) {
    return 2;
  } else {
    return 2;
  }
}

function cmaxTable(dose,tau) {
  return dose*(1-Math.exp(-1*k()*tinfTable(dose))) / (tinfTable(dose) * vd() * k() * (1-Math.exp(-1*k()*tau)));
}

function cminTable(dose,tau) {
  return cmaxTable(dose,tau) * Math.exp(-1*k()*(tau - tinfTable(dose)));
}

function aucTable(dose,tau) {
  return ((24/tau) * ((cmaxTable(dose,tau) + cminTable(dose,tau))*tinfTable(dose)/2 + ((cmaxTable(dose,tau) - cminTable(dose,tau))/k())));
}

function stitchCell(dose, tau) {
  var x = cmaxTable(dose,tau).toFixed(1);
  var y = cminTable(dose,tau).toFixed(1);
  var z = aucTable(dose,tau).toFixed(0)
  return x + " / " + y + " <br /> " + z;
}

function updateTableCell(dose,tau,id) {
  pushText(stitchCell(dose,tau),id);
  if (aucTable(dose,tau) >= 400 && aucTable(dose,tau) <= 600) {
    document.getElementById(id).setAttribute("style", "background-color:var(--success);");
  } else if ((aucTable(dose,tau) >= 380 && aucTable(dose,tau) < 400) || (aucTable(dose,tau) <= 620 && aucTable(dose,tau) > 600)) {
    document.getElementById(id).setAttribute("style", "background-color:var(--warning);");
  } else {
    document.getElementById(id).setAttribute("style", "background-color:var(--danger);");
  }
}

function updateTable() {
  updateTableCell(750,8,'75q8');
  updateTableCell(1000,8,'1q8');
  updateTableCell(1250,8,'125q8');
  updateTableCell(1500,8,'15q8');
  updateTableCell(1750,8,'175q8');
  updateTableCell(2000,8,'2q8');

  updateTableCell(750,12,'75q12');
  updateTableCell(1000,12,'1q12');
  updateTableCell(1250,12,'125q12');
  updateTableCell(1500,12,'15q12');
  updateTableCell(1750,12,'175q12');
  updateTableCell(2000,12,'2q12');

  updateTableCell(750,18,'75q18');
  updateTableCell(1000,18,'1q18');
  updateTableCell(1250,18,'125q18');
  updateTableCell(1500,18,'15q18');
  updateTableCell(1750,18,'175q18');
  updateTableCell(2000,18,'2q18');

  updateTableCell(750,24,'75q24');
  updateTableCell(1000,24,'1q24');
  updateTableCell(1250,24,'125q24');
  updateTableCell(1500,24,'15q24');
  updateTableCell(1750,24,'175q24');
  updateTableCell(2000,24,'2q24');

  updateTableCell(750,36,'75q36');
  updateTableCell(1000,36,'1q36');
  updateTableCell(1250,36,'125q36');
  updateTableCell(1500,36,'15q36');
  updateTableCell(1750,36,'175q36');
  updateTableCell(2000,36,'2q36');

  updateTableCell(750,48,'75q48');
  updateTableCell(1000,48,'1q48');
  updateTableCell(1250,48,'125q48');
  updateTableCell(1500,48,'15q48');
  updateTableCell(1750,48,'175q48');
  updateTableCell(2000,48,'2q48');
}
